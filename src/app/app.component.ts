import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'basic-routing';

  firstName = "Pranav Sharma";
  secondName = "Vikram Singh";
  thirdName = "Jenish Kumar";
}
