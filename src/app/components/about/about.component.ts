import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs';
import { BookService } from 'src/app/book.service';
import { Book } from '../../models/book';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  bookName: string = '';
  bookDetails?: Book;

  constructor(private route: ActivatedRoute, private bookService: BookService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((param: ParamMap) => {
      const bookId = param.get('bookId');
      console.log(bookId);
      this.bookService.getBookDetails(bookId)
      .subscribe((bookDetails: Book) => {
        this.bookDetails = bookDetails;
        console.log(this.bookDetails);
      })
    })
    //---------------------------------
    // this.route.paramMap
    // .pipe(
    //   switchMap((param: ParamMap) => {
    //     const bookId = param.get('bookId');
    //     return this.bookService.getBookDetails(bookId);
    //   })
    // )
    // .subscribe((bookDetails: Book) => {
    //   this.bookDetails = bookDetails;
    // })
  }

  //===========================
  // ngOnInit(): void {
  //   this.route.paramMap.subscribe((param: ParamMap) => {
  //     const bookName = param.get('bookName');
  //     console.log(bookName);  
  //     this.bookService.getBookDetails(bookName)
  //     .subscribe((bookDetails: any) => {
  //       console.log(bookDetails);
  //     })
  //   });
  // }

  ///---------------------------------------------------

  // ngOnInit(): void {
  //   // console.log(this.route);
  //   this.route.params.subscribe((value) => {
  //     // console.log(value); 
  //   })
  //   this.route.params.subscribe((value: any) => {
  //     console.log(value['bookName']);
  //     // this.bookName = value['bookName']; 
  //     this.bookName = value.bookName;
  //   });
  //   this.route.paramMap.subscribe((value: ParamMap) => {
  //     // console.log(value.get('bookName'));
  //   })
  // }
}
