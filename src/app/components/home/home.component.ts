import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Book } from 'src/app/models/book';
import { BookService } from 'src/app/book.service';
import { filter, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  books: Book[] = [];

  constructor(private router: Router, private bookService: BookService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    // this.route.queryParamMap
    // .subscribe((value: ParamMap) => {
    //   // console.log(value.get('q'))
    //   const searchTxt = value.get('q');
    //   if(searchTxt){
    //     this.bookService.searchBook(searchTxt)
    //     .subscribe((books: Book[]) => {
    //       this.books = books;
    //     })
    //   }
    // })
    //--------------------------------------------
    this.route.queryParamMap
    .pipe(
      filter((params: ParamMap) => params.get('q') !== null),
      switchMap((param: ParamMap) => {
        return this.bookService.searchBook(param.get('q'))
      })
    )
    .subscribe((books: Book[]) => {
      this.books = books;
    })
  }

  
  onSearch(txt: string){
    this.router.navigateByUrl(`/?q=${txt}`);    
  }


  // onSearch(txt: string){
  //   this.bookService.searchBook(txt)
  //   .subscribe((books: Book[]) => {
  //     console.log(books);
  //     this.books = books;
  //   })
  // }

  onLogin(txt: string){
    if(txt.includes('admin')){
      this.router.navigateByUrl('/about');
    }
    else{
      this.router.navigateByUrl('/contact-us');
    }
  }
}
