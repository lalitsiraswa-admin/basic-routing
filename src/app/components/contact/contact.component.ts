import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe((value) => {
      console.log(value);
    })
  }
  goBack(){
    // console.log(this.router);
    // console.log(this.router.url);
    this.router.navigateByUrl("/");
  }
}
